/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  PermissionsAndroid,
  NativeModules
  
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const { TrustScorePlugin : CVBridge } = NativeModules;

CV_CONSTANTS=  {
  MOBILE_NUMBER: "MOBILE_NUMBER",
  EMAIL_ID: "EMAIL_ID",
};


const creditVidya = {
  apiKey:"cHON8KAZiCE8ar13K28TxrrKAIi3Gd7F",
  environment:"UAT",
  permissions:{
    required: [
      'android.permission.READ_SMS',
      'android.permission.GET_ACCOUNTS',
      'android.permission.RECEIVE_BOOT_COMPLETED',
      'android.permission.ACCESS_NETWORK_STATE',
      // 'android.permission.ACCESS_COARSE_UPDATES', //TODO need to check with credit vidya
      'android.permission.ACCESS_WIFI_STATE',
      'android.permission.READ_PHONE_STATE',
    ],
    optional: [
      'android.permission.READ_CONTACTS',
      'android.permission.ACCESS_FINE_LOCATION',
      'android.permission.ACCESS_COURSE_LOCATION',
    ],
  },
}
export const initializeCreditVidya = async() =>{
  const deviceId = '',  email = 'k@k.com', mobileNumber = '8126319135';

  await CVBridge.initialize();
  CVBridge.register(
    deviceId,
    creditVidya.apiKey,
    creditVidya.environment,
    (success: boolean, statusCode: number, message: string) => {
      console.log('creditVidya', statusCode, success, message);
      if (success) {
        !!email && CVBridge.sendUserData(CV_CONSTANTS.EMAIL_ID, email);
        !!mobileNumber && CVBridge.sendUserData(CV_CONSTANTS.MOBILE_NUMBER, `${mobileNumber}`);
      }
    }
  );
};

export const registerCreditVidya = async () => {
  const requiredPermissionResult = await PermissionsAndroid.requestMultiple(
    creditVidya.permissions.required
  );
  const result = Object.keys(requiredPermissionResult).reduce(
    (result: boolean, current: string) => {
      return result && requiredPermissionResult[current] === 'granted';
    },
    true
  );
  let xiaomiSmsResult = true;
  if (requiredPermissionResult['android.permission.READ_SMS'] !== 'granted') {
    xiaomiSmsResult = await CVBridge.showServiceSmsPermissionSettings();
  }
  if (!!result && xiaomiSmsResult) {
    await CVBridge.requestPermissions(creditVidya.permissions.optional);
    await initializeCreditVidya()
    return true;
  }
  return false;
};

const startCreditVidya =async()=>{
  await registerCreditVidya();
}

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Header />
          
          <Button style={{height:50, marginHorizontal:16, }}  onPress={startCreditVidya} title='Initialize Credit Vidya'/>
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Step One</Text>
              <Text style={styles.sectionDescription}>
                Edit <Text style={styles.highlight}>App.js</Text> to change this
                screen and then come back to see your edits.
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>See Your Changes</Text>
              <Text style={styles.sectionDescription}>
                <ReloadInstructions />
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Debug</Text>
              <Text style={styles.sectionDescription}>
                <DebugInstructions />
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Learn More</Text>
              <Text style={styles.sectionDescription}>
                Read the docs to discover what to do next:
              </Text>
            </View>
            <LearnMoreLinks />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
