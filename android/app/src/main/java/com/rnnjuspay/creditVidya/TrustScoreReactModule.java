package com.rnnjuspay.creditVidya;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;


import com.cv.sdk.RegistrationParams;
import com.cv.sdk.TrustScoreHelper;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import static android.app.Activity.RESULT_OK;

public class TrustScoreReactModule extends ReactContextBaseJavaModule implements ActivityEventListener {

    private static final String DATA_EMAIL_ID = "EMAIL_ID";
    private static final String DATA_MOBILE_NUMBER = "MOBILE_NUMBER";
    private static final int RC_SERVICE_SMS_SETTINGS = 1222;

    private ReactContext reactContext;
    private TrustScoreHelper trustScoreHelper;
    private Promise miSmsPermission;

    public  TrustScoreReactModule(ReactApplicationContext reactContext){
        super(reactContext);
        this.reactContext = reactContext;
    }
    private TrustScoreHelper getSdkInstance(){
        return TrustScoreHelper.Factory.get(getCurrentActivity());
    }
    @ReactMethod
    public void initialize(){
        getSdkInstance().initialize();
    }

    @ReactMethod
    public  void register(String loginName, String apiKey, String environment, final Callback callback){
        final RegistrationParams trustScoreParams = new RegistrationParams()
                .apiKey(apiKey)
                .environment(environment)
                .loginName(loginName)
                .completionCallback(new TrustScoreHelper.RegistrationCallback() {
                    @Override
                    public void onRegistered(boolean success, int statusCode, @NotNull String message) {
                        callback.invoke(success, statusCode, message);
                    }
                });
        getSdkInstance().register(trustScoreParams);
    }
    @ReactMethod
    public void sendUserData(String key, String value) {
        getSdkInstance().sendUserData(key, value);
    }
    boolean isXiaomiPhone() {
        return "xiaomi".equalsIgnoreCase(Build.MANUFACTURER);
    }

    /***  Shows a new window where user has to manually check "Service SMS".*  When user closes that window, onActivityResult is invoked with resultcode = RC_SERVICE_SMS_SETTINGS*/
    @ReactMethod
    void showServiceSmsPermissionSettings(Promise promise) {
        if (!isXiaomiPhone() || getCurrentActivity() == null) {
            promise.resolve(true);
            return;
        }
        miSmsPermission = promise;
        try {// MIUI 8
            Intent localIntent = new Intent("miui.intent.action.APP_PERM_EDITOR");
            localIntent.setClassName("com.miui.securitycenter", "com.miui.permcenter.permissions.PermissionsEditorActivity");
            localIntent.putExtra("extra_pkgname", getReactApplicationContext().getPackageName());
            getCurrentActivity().startActivityForResult(localIntent, RC_SERVICE_SMS_SETTINGS);
        } catch (Exception e) {
            e.printStackTrace();
            try {// MIUI 5/6/7
                Intent localIntent = new Intent("miui.intent.action.APP_PERM_EDITOR");
                localIntent.setClassName("com.miui.securitycenter", "com.miui.permcenter.permissions.AppPermissionsEditorActivity");
                localIntent.putExtra("extra_pkgname", getReactApplicationContext().getPackageName());
                getCurrentActivity().startActivityForResult(localIntent, RC_SERVICE_SMS_SETTINGS);
            } catch (Exception e1) {
                e1.printStackTrace();// Otherwise jump to application details
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getReactApplicationContext().getPackageName(), null);
                intent.setData(uri);
                getCurrentActivity().startActivityForResult(intent, RC_SERVICE_SMS_SETTINGS);
            }
        }
    }
    @ReactMethod
    public void requestPermissions(ReadableArray permissionsArray, Promise promise) {
        String[] permissionsString = new String[permissionsArray.size()];
        for (int index = 0; index < permissionsArray.size(); index++) {
            permissionsString[index] = permissionsArray.getString(index);
        }
        if (getCurrentActivity() != null) {
            ActivityCompat.requestPermissions(getCurrentActivity(), permissionsString, 1);
            promise.resolve(true);
        } else {
            promise.resolve(false);
        }
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants () {
        final Map<String, Object> constants = new HashMap<>();
        constants.put(DATA_MOBILE_NUMBER, TrustScoreHelper.MOBILE_NUMBER);
        constants.put(DATA_EMAIL_ID, TrustScoreHelper.EMAIL_ID);
        return constants;
    }
    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SERVICE_SMS_SETTINGS) {
            if (resultCode == RESULT_OK) {
                miSmsPermission.resolve(true);
            } else {
                miSmsPermission.reject("PERMISSION", "REJECT");
            }
        }
    }

    @Override
    public void onNewIntent(Intent intent) {

    }

    @NonNull
    @NotNull
    @Override
    public String getName() {
        return "TrustScorePlugin";
    }
}
